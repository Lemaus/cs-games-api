﻿using CRM_API.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CRM_API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class CsGamesController : ControllerBase
    {
        private readonly CRMContext _context;
        
        public CsGamesController(CRMContext context)
        {
            _context = context;
        }

        private readonly string[] _konamiCode = {
            "ArrowUp",
            "ArrowUp",
            "ArrowDown",
            "ArrowDown",
            "ArrowLeft",
            "ArrowRight",
            "ArrowLeft",
            "ArrowRight",
            "b",
            "a"
        };

        [HttpPost("Un")]
        public ActionResult ValidateFirst([FromBody]string answer)
        {
            if (answer == "∞")
                return Ok();

            return Conflict();
        }

        [HttpPost("Deux")]
        public ActionResult ValidateSecond(string[] answer)
        {
            if (answer.SequenceEqual(_konamiCode))
                return Ok();

            return Conflict();
        }

        [HttpPost("Trois")]
        public ActionResult ValidateSecond([FromBody]string answer)
        {
            answer = answer.Trim().Replace("|", ";").Replace("-", ";").Replace(" ", ";").Replace(",", ";");
            if (string.Equals(answer, "NH6;QG8;NF7", StringComparison.InvariantCultureIgnoreCase))
                return Ok();

            return Conflict();
        }

        [HttpGet("Leaderboard")]
        public ActionResult GetLeaderboard()
        {
            //return Ok(_context.Leaderboards);
            return Ok();
        }

        [HttpGet("Teams")]
        public ActionResult GetTeams()
        {
            return Ok(_context.Teams);
            return Ok();
        }

        [HttpPost("Prize")]
        public ActionResult SubmitAnswer(AnswerForm answer)
        {
            Prize prize = _context.Prizes.FirstOrDefault(p => p.Puzzle == answer.Puzzle && p.Answer == answer.Answer);

            if (prize == null) return Conflict();

            _context.TeamPrizes.Add(new TeamPrize
            {
                PrizeId = prize.Id,
                TeamId = answer.TeamId,
            });
            _context.SaveChanges();

            return Ok();
        }

        public class AnswerForm
        {
            public int Puzzle { get; set; }
            public string Answer { get; set; }
            public int TeamId { get; set; }
        }
    }
}
