﻿using System;
using System.Collections.Generic;

namespace CRM_API.Models;

public partial class Leaderboard
{
    public long? TotalScore { get; set; }

    public string TeamName { get; set; }

    public int? TeamId { get; set; }
}
