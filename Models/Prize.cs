﻿using System;
using System.Collections.Generic;

namespace CRM_API.Models;

public partial class Prize
{
    public int Puzzle { get; set; }

    public string Answer { get; set; }

    public int Score { get; set; }

    public int Id { get; set; }

    public virtual ICollection<TeamPrize> TeamPrizes { get; } = new List<TeamPrize>();
}
