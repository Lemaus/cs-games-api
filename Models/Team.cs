﻿using System;
using System.Collections.Generic;

namespace CRM_API.Models;

public partial class Team
{
    public int Id { get; set; }

    public string Name { get; set; }

    public virtual ICollection<TeamPrize> TeamPrizes { get; } = new List<TeamPrize>();
}
