﻿using System;
using System.Collections.Generic;

namespace CRM_API.Models;

public partial class TeamPrize
{
    public int Id { get; set; }

    public int TeamId { get; set; }

    public int PrizeId { get; set; }

    public virtual Prize Prize { get; set; }

    public virtual Team Team { get; set; }
}
