﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace CRM_API.Models;

public partial class CRMContext : DbContext
{
    public CRMContext()
    {
    }

    public CRMContext(DbContextOptions<CRMContext> options)
        : base(options)
    {
    }

    public virtual DbSet<Leaderboard> Leaderboards { get; set; }

    public virtual DbSet<Prize> Prizes { get; set; }

    public virtual DbSet<Team> Teams { get; set; }

    public virtual DbSet<TeamPrize> TeamPrizes { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        => optionsBuilder.UseNpgsql("Name=ConnectionStrings:CRM_DB");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Leaderboard>(entity =>
        {
            entity
                .HasNoKey()
                .ToView("leaderboard", "csgames");

            entity.Property(e => e.TeamId).HasColumnName("team_id");
            entity.Property(e => e.TeamName).HasColumnName("team_name");
            entity.Property(e => e.TotalScore).HasColumnName("total_score");
        });

        modelBuilder.Entity<Prize>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("prize_pk");

            entity.ToTable("prize", "csgames");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Answer)
                .IsRequired()
                .HasColumnType("character varying")
                .HasColumnName("answer");
            entity.Property(e => e.Puzzle).HasColumnName("puzzle");
            entity.Property(e => e.Score).HasColumnName("score");
        });

        modelBuilder.Entity<Team>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("team_pk");

            entity.ToTable("team", "csgames");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.Name)
                .IsRequired()
                .HasColumnType("character varying")
                .HasColumnName("name");
        });

        modelBuilder.Entity<TeamPrize>(entity =>
        {
            entity.HasKey(e => e.Id).HasName("team_prize_pk");

            entity.ToTable("team_prize", "csgames");

            entity.Property(e => e.Id).HasColumnName("id");
            entity.Property(e => e.PrizeId).HasColumnName("prize_id");
            entity.Property(e => e.TeamId).HasColumnName("team_id");

            entity.HasOne(d => d.Prize).WithMany(p => p.TeamPrizes)
                .HasForeignKey(d => d.PrizeId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("team_prize_fk2");

            entity.HasOne(d => d.Team).WithMany(p => p.TeamPrizes)
                .HasForeignKey(d => d.TeamId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("team_prize_fk");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
